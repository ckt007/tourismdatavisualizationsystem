import requests
from lxml import etree
import re
import json
import csv
import os
import pandas as pd
import time
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE','biyesheji.settings')
django.setup()
from app.models import TravelInfo

class spider(object):
    def __init__(self):
        self.url = 'https://piao.qunar.com/ticket/list.json?keyword=%s&page=%s'
        self.detailUrl = 'https://piao.qunar.com/ticket/detail_%s.html'
        self.commentUrl = 'https://piao.qunar.com/ticket/detailLight/sightCommentList.json?sightId=%s&pageSize=10&index=1'
        self.headers = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36 Edg/123.0.0.0',
        'Cookie':'SECKEY_ABVK=pnBqXh6DU4+DPXOZu06DnKnrlPCjXMvfgV4Mvf74PaM%3D; BMAP_SECKEY=2JGEgkmDOi7_dbeZHv-QSIddCQBKovsyztsnwWZ7kGPO3bUXs8gGFKRApbOZ_xaP645XPr7Rw4e6BWxCjYG8bRY7iaE1phGN5PDSDUT4T0mV1magc5wKuhfzGGpapiFsFADueWUAJs6aPcNCDCqilCoqVYl0VH_Vs-jtdR9jXqDzE12fNLDBY7rCCBu9sv84BoIr7YcEzjBc_Z2YY3cPLg; QN1=0000eb80306c5e0c4778ab87; QN300=s%3Dbaidu; QN99=7200; QunarGlobal=10.68.47.32_-7142fe65_18e9a23bf89_74dd|1711997679305; QN205=s%3Dbaidu; QN277=s%3Dbaidu; QN601=2e259028b51f3752ab855b58a80150b9; QN269=4B13ABC2F05911EE8A02BEAB2A4BB230; QN48=0000f6002f105e0c47782754; fid=a9f7a27d-5ce0-4e82-9c2b-256a3515c264; ctt_june=1683616182042##iK3wWSt%3DWUPwawPwasWIVRa8EKg8EKv%2BVKoTWDX%3DWR3%2BVR38XPXNWDjOXstniK3siK3saKgnaSv8WK3AWsD%2BVhPwaUvt; ctf_june=1683616182042##iK3waKgOWuPwawPwasHRaSGRaP3nESoGVPiIVRohEDDwaRa%2BEDWIWPD8VRfRiK3siK3saKgnaSv8WK3AWsDNahPwaUvt; ariaDefaultTheme=null; cs_june=4d27b1cde32fe56004904bccbde02a242562050a6d26a8a21a47e73dd269fb77738e1d74047357b76ac575809c37a68c31c2d4b1d433738b6c0c5103beea8284b17c80df7eee7c02a9c1a6a5b97c1179d36b4900679ceae736e7b5094412444e5a737ae180251ef5be23400b098dd8ca; QN271AC=register_pc; QN271SL=d7add10e73dd408bf446f2b34565d951; QN271RC=d7add10e73dd408bf446f2b34565d951; _q=U.cgvzpam9464; csrfToken=qCxzeKxkYsPkEPGymvo23E4cNCBrdYyp; _s=s_5HWQJY63W7MJYFMX6DL3IWQBOY; _t=28642861; _v=IWTMgZi7nPqYJw-yZzW4bbPDw6agezAJfimRPRPBjI3Ao3mMpwCY6xsbiSmuXcM_6J6RxnBNz-rSArPns8UeoqYnwfnCl9gnPPGHtUcD6WeG-6M0g9-jwkmvpRRROzkMUm7-ec4rFyAHJdcGAC5fdiAG5JwkI0z7XB4pTbmBHJMi; QN43=""; QN42=%E5%8E%BB%E5%93%AA%E5%84%BF%E7%94%A8%E6%88%B7; _i=VInJO_KPWR1qlWK1YHE5HvHsYKRq; QN57=17120053089430.26972742631678903; QN71="MTgzLjIwOS4xNTkuNzI65YyX5LqsOjE="; QN63=%E5%8C%97%E4%BA%AC; qunar-assist={%22version%22:%2220211215173359.925%22%2C%22show%22:false%2C%22audio%22:false%2C%22speed%22:%22middle%22%2C%22zomm%22:1%2C%22cursor%22:false%2C%22pointer%22:false%2C%22bigtext%22:false%2C%22overead%22:false%2C%22readscreen%22:false%2C%22theme%22:%22default%22}; QN44=cgvzpam9464; _vi=IG1lrAvFUyPlYDHgE7bjz0mBRZb-jF0UkgnzoAuP-ePcrrItSyIgE3R1XzjVuvCJVsvkhPG3XaBeLCrfe_NckHHG9KoeIMlwoJpQQJ6JX7goUzDFBoh7BEXmgkzcRcs7wRZG562G3RQzSWZaeY62jnarSsuAPXokcW6l4CJJwqSh; QN67=518071%2C9742%2C14161; QN58=1712053975938%7C1712054893036%7C8; JSESSIONID=B2889BBB6283605576406123590812F8; QN271=7693797a-95e3-49f8-b78c-ed6e669575e5; QN267=0940582666ebe2703; __qt=v1%7CVTJGc2RHVmtYMS9ERTFLelJ4OWU3d1NBVnMwcG5qRHRmRllrK0dWZjdyK3h5SHB6QmQvNXRiaXdaZzRxb3pPS1cxOXlzR0VUN1VCN2JNSVlkeTlGSlZrMzBVbk9SSlVCbGlGTElmZzdBaXFhUEFjWkRFSFB6L2RDdnFDVEhpbTZodFVXS2VPaUVBMEthditwdFg2ODc1a05GZEgzUnF6b1l3QjYrR0lvRk9nalZKelEvZGNaYlZudlhWbi9nNjlF%7C1712054900448%7CVTJGc2RHVmtYMSs5U3RtRldCMVpNeTFNMkRyMWFrMkhxOWhlWkJ3OUJjSWxBSTRudXJjVFZQMk1uc0lXNm1CMXVSWjQ4dGhGZ1gweFFkWEFGTE9YSnc9PQ%3D%3D%7CVTJGc2RHVmtYMSt3cEo0NXhBbUh6RityZDdXSXBrT0VpSDJlWGltUFRtclpycHpKNDU0bDZSenNyTVh4QnoyaFhlbXBKRkFKUStxcjFDU0wrdFZwcEoyNTc1ZlgxeDNnLzZuVFlraE92blU2TlJnTnhvNTliUExxa2MvUkpieG5QUzZOOHowUkNrSkpDMHlyK0ZDWmhRblFQZWRyMEdtZ012amNNV3RYcVB1QWwwZXlRNmsxRkxhODBrckVQR3I0c2lQd0lHM0VjcGJyM2lqVTNwQUFSRzVheGRWdHJja2lTQVpGakw4b1lvWGVOVk5xQmIxVmJEZ2hXWmtINGpEMkJOYVk1S0E4YU0xcllOUmJzaTlGMjZlbm5Gekg1c0NMME9wNVlSL0Q2WTJIOVdPTmlmcDdNa1BFbXVPcUJrQjdxTVhtQjZ3MExmdEdtd0tINHVrdXdjZUN5cDgxWDdBTTNNTVRvV0tHZkNxd0Y2VjMwZ083bk9DU243RXQ2NTc2aURYbnZ0SHZ2ZVU5Mlppd0lxRG1XMWR5NSs5U0txVzFpSU5INjdQR3VjeWN1ZFB2VCswdlFTNmZmaExTbXBvQXFVSFpzL3gyc3lWYXd2R0d6N2R4RWkveDFVYURkRVZHU0tCSDlTWkZGMlJHOGVIUzN2UFJoOTFiN2JvcVJvQUY3MXhWUnc0YzVKSENxRXE1TnVBT24venZwVlpmaElJY1pCaXR0U2tSN1FtZDB6Qm9HUEdwTUVlV3JkSU9tR2xQN2NvQ1NPUTY3YmI3L3BBQmZMSVkrcEN0aSs4RHc4NDRvRXlLMnlsM3NDK0JLN2xWbStPQlkwSWpyZnN2OUMwb1NzVjhJalQ0b3hCdFBvT2hjZVA2dHlDWVlSV0d5NFY5MkJiZkxJUlNNaHE1UUQzd3lCcFg4cUFFK25WSlNHN2dFUDgyNTFTelNneHJYU3FsOFQ1MlYwVXV1M2JaOHhzb0psdzl0TFowWXg5N2lZcTFqNVVlRWx4WnJoQ0Zjd1Rk'
    }

    def init(self):
        if not os.path.exists('tempData.csv'):
            with open('tempData.csv', 'w', encoding='utf8', newline='') as csvfile:
                wirter = csv.writer(csvfile)
                wirter.writerow([
                    'sightName',
                    'star',
                    'city',
                    'detailAddress',
                    'shortIntro',
                    'detailIntro',
                    'detailUrl',
                    'price',
                    'discount',
                    'sale_count',
                    'score',
                    'commentsTotal',
                    'comments',
                    'cover_img',
                    'img_list',
                ])

    def send_request(self,url):
        response = requests.get(url, headers=self.headers)
        if response.status_code == 200:
            return response
        else:
            return None

    def save_to_csv(self,row):
        with open('tempData.csv','a',encoding='utf8',newline='') as csvfile:
            wirter = csv.writer(csvfile)
            wirter.writerow(row)

    def save_to_sql(self):
        with open('tempData.csv','r',encoding='utf8') as csvfile:
            readerCsv = csv.reader(csvfile)
            next(readerCsv)
            for travel in readerCsv:
                try:
                    TravelInfo.objects.create(
                        sightName=travel[0],
                        star=travel[1],
                        city=travel[2],
                        detailAddress=travel[4],
                        shortIntro=travel[5],
                        detailIntro=travel[10],
                        detailUrl=travel[6],
                        price=travel[8],
                        discount=travel[14],
                        sale_count=travel[15],
                        score=travel[7],
                        commentsTotal=travel[9],
                        comments=travel[12],
                        cover_img=travel[13],
                        img_list=travel[11],
                    )
                except:
                    continue


    def spiderMain(self,resp,city):
        respJSON = resp.json()['data']['sightList']
        for index,travel in enumerate(respJSON):
            print('正在爬取该页第%s数据' % str(index + 1))
            time.sleep(2)
            detailAddress = travel['address']
            discount = travel['discount']
            shortIntro = travel['intro']
            price = travel['qunarPrice']
            sale_count = travel['saleCount']
            try:
                star = travel['star'] + '景区'
            except:
                star = '未评价'
            sightName = travel['sightName']
            cover_img = travel['sightImgURL']
            sightId = travel['sightId']
            # ================================= 详情爬取
            detailUrl = self.detailUrl % sightId
            respDetailXpath = etree.HTML(self.send_request(detailUrl).text)
            score = respDetailXpath.xpath('//span[@id="mp-description-commentscore"]/span/text()')
            if not score:
                score = 0
            else:
                score = score[0]
            commentsTotal = respDetailXpath.xpath('//span[@class="mp-description-commentCount"]/a/text()')[0].replace('条评论','')
            detailIntro = respDetailXpath.xpath('//div[@class="mp-charact-intro"]//p/text()')[0]
            img_list = respDetailXpath.xpath('//div[@class="mp-description-image"]/img/@src')[:6]
            # ================================= 评论爬取
            commentSightId = respDetailXpath.xpath('//div[@class="mp-tickets-new"]/@data-sightid')[0]
            commentsUrl = self.commentUrl % commentSightId
            comments = []
            try:
                commentsList = self.send_request(commentsUrl).json()['data']['commentList']
                for c in commentsList:
                    if c['content'] != '用户未点评，系统默认好评。':
                        author = c['author']
                        content = c['content']
                        date = c['date']
                        score = c['score']
                        comments.append({
                            'author': author,
                            'content': content,
                            'date': date,
                            'score': score
                        })
            except:
                comments = []

            resultData = []
            resultData.append(sightName)
            resultData.append(star)
            resultData.append(city)
            resultData.append(detailAddress)
            resultData.append(shortIntro)
            resultData.append(detailIntro)
            resultData.append(detailUrl)
            resultData.append(price)
            resultData.append(discount)
            resultData.append(sale_count)
            resultData.append(score)
            resultData.append(commentsTotal)
            resultData.append(json.dumps(comments))
            resultData.append(cover_img)
            resultData.append(json.dumps(img_list))
            self.save_to_csv(resultData)


    def start(self):
        with open('./city.csv','r',encoding='utf8') as readerFile:
            readerCsv = csv.reader(readerFile)
            next(readerCsv)
            for cityData in readerCsv:
                for page in range(1,100):
                    try:
                        url = self.url % (cityData[0], page)
                        print('正在爬取 %s 该城市的旅游数据正在第 %s 页 路径为: %s' % (
                            cityData[0],
                            page,
                            url
                        ))

                        response = self.send_request(url)
                        self.spiderMain(response, cityData[0])
                        time.sleep(3)
                    except:
                        continue


if __name__ == '__main__':
    spiderObj = spider()
    # spiderObj.init()
    # spiderObj.start()
    spiderObj.save_to_sql()

