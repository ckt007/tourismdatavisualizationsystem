import requests
from lxml import etree
import csv
import os

def init():
    if not os.path.exists('city.csv'):
        with open('city.csv','w',encoding='utf8',newline='') as csvfile:
            wirter = csv.writer(csvfile)
            wirter.writerow([
                'city',
                'cityLink'
            ])

def wirterRow(row):
        with open('city.csv','a',encoding='utf8',newline='') as csvfile:
            wirter = csv.writer(csvfile)
            wirter.writerow(row)

def get_html(url):
    headers  = {
        'User-Agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36 Edg/123.0.0.0',
        'Cookie':'QN1=0000eb80306c5e0c4778ab87; QN300=s%3Dbaidu; QN99=7200; QunarGlobal=10.68.47.32_-7142fe65_18e9a23bf89_74dd|1711997679305; qunar-assist={%22version%22:%2220211215173359.925%22%2C%22show%22:false%2C%22audio%22:false%2C%22speed%22:%22middle%22%2C%22zomm%22:1%2C%22cursor%22:false%2C%22pointer%22:false%2C%22bigtext%22:false%2C%22overead%22:false%2C%22readscreen%22:false%2C%22theme%22:%22default%22}; QN205=s%3Dbaidu; QN277=s%3Dbaidu; QN601=2e259028b51f3752ab855b58a80150b9; QN163=0; QN269=4B13ABC2F05911EE8A02BEAB2A4BB230; QN48=0000f6002f105e0c47782754; fid=a9f7a27d-5ce0-4e82-9c2b-256a3515c264; ctt_june=1683616182042##iK3wWSt%3DWUPwawPwasWIVRa8EKg8EKv%2BVKoTWDX%3DWR3%2BVR38XPXNWDjOXstniK3siK3saKgnaSv8WK3AWsD%2BVhPwaUvt; ctf_june=1683616182042##iK3waKgOWuPwawPwasHRaSGRaP3nESoGVPiIVRohEDDwaRa%2BEDWIWPD8VRfRiK3siK3saKgnaSv8WK3AWsDNahPwaUvt; ariaDefaultTheme=null; cs_june=4d27b1cde32fe56004904bccbde02a242562050a6d26a8a21a47e73dd269fb77738e1d74047357b76ac575809c37a68c31c2d4b1d433738b6c0c5103beea8284b17c80df7eee7c02a9c1a6a5b97c1179d36b4900679ceae736e7b5094412444e5a737ae180251ef5be23400b098dd8ca; QN271AC=register_pc; QN271SL=d7add10e73dd408bf446f2b34565d951; QN271RC=d7add10e73dd408bf446f2b34565d951; _q=U.cgvzpam9464; csrfToken=qCxzeKxkYsPkEPGymvo23E4cNCBrdYyp; _s=s_5HWQJY63W7MJYFMX6DL3IWQBOY; _t=28642861; _v=IWTMgZi7nPqYJw-yZzW4bbPDw6agezAJfimRPRPBjI3Ao3mMpwCY6xsbiSmuXcM_6J6RxnBNz-rSArPns8UeoqYnwfnCl9gnPPGHtUcD6WeG-6M0g9-jwkmvpRRROzkMUm7-ec4rFyAHJdcGAC5fdiAG5JwkI0z7XB4pTbmBHJMi; QN43=""; QN42=%E5%8E%BB%E5%93%AA%E5%84%BF%E7%94%A8%E6%88%B7; QN44=cgvzpam9464; _i=VInJO_KPWR1qlWK1YHE5HvHsYKRq; QN57=17120053089430.26972742631678903; QN71="MTgzLjIwOS4xNTkuNzI65YyX5LqsOjE="; QN63=%E5%8C%97%E4%BA%AC; _vi=PmAt7zjFXt0EvIQ-HYg7ouUIe3Yw5HCaatyDAJYnAPfxB7ujIyoWEOPzolREjcVRXfawOITfSHr4xjFsIWdPnkRHNYMnVB0n53A0m9URrtlZ45NGgpxJM-_g4yaPDdOC6h-IkiE89aRJKrkHoU8kvpNhjjMBNzYmfH0VsLXJd7pq; QN267=0940582668649293b; QN58=1712005308942%7C1712006771061%7C24; QN271=9175675d-9970-43a5-83ce-708eefa10615; JSESSIONID=B03C736B10B752CCD460F214D4B337C7; __qt=v1%7CVTJGc2RHVmtYMTgzcEpncFhQNGkvYk9SQmRGQTFwMXRwRllpVXlkZW1TQlpnUlFtb1N2UEpNMmpRQ1BMWWlnYnFrOHluUDNwTWNPUm9kYUtwZFAvbU1nSG8xQVMrVXZkakphWkhpc0xuTDZZQ1BZMm1leWJSV3J3UEpBc2tmd0dNeXMwRmcyaDQranBNYWJQNUxZMlVqUjhVTHFZY1ZzaTZyR0RqYzlyeFowNlNKS1FnejBXbWhzWWt2d0d0ZlB1%7C1712006862067%7CVTJGc2RHVmtYMSt4TTN6dXk4VDNaYWhnbXk5SGgwS3I2Y2MvVEN6Q09PZDIxNzFFdEtST3doV3UybTg0T2Rla2haRW1kd29hTG1tNkNjTkY4cG9nNlE9PQ%3D%3D%7CVTJGc2RHVmtYMStZNjN3dVpieVRSWWxIdzQxSVA5WjRHWUFPU25FdU53VnVKMFRkSnhMcHRua0JLR3orM05ObmIyUStubWVTV0d3N0Z4bUw5bnRTczRmL0hmcWxQdHZ2TVVIQzlOa3c5THIvYUw2Y0V1QVhWNHBYbWpiK3lJMHo4VzhDeEJWbEM2dDFhOW1UR3FaYlc4bGlGNFBXbkhuRjM3WGhLckd0NTVvOTRrVXBubDd2NnF1K0M0d3Q0ZzExZ1ZLcTJHSTRnQnppZGxPLzhEN2JiYXBGVXVaRDk4VDBMT2pvQTUrYXUyR1hYOWd6dlIvV3NmYW5rWnZkaG1JUE96eHMrelR1SGVDTDVEenNHZmd5T0dUdVZOUHh2Tjgramg5RFhEZkpNckRya3lSTVlRaFRRKzhHT2FQdGdETHBnTXJuTzZ4cUJpK1Q0TVNqMm8vM250REx5R1pxL1V5QlY1cjFlcjU5SVJjbm90bm1DSmRjWW5abVIzV2RRaGhXQnk5RWMvYmJMSVUzdk80am1HM0tTamUzcEltbzdySFc2Nk0yS3krQXR5WnQ3Mm4zSDU2TmVZUTkzR2N3OUpFNVUxbFJkR0tOaW1wWE4wbWhnWjB6T2ord2NDZHIvakRyK2JhS0VKYkR3ZXE1MitlWTBpNnFhMXVhUlhoUTZwTUdvMVluKzVuTFJvNWtiMVl0aGgzT2F0eFZwam13QzJJcTdrUW52L3ZPQjdFMUl2dEhKMTF5Wkt0R2xxWmpPU2xUaTF6dTF6VGxadnVXb291d2IxNXM1RStOU2p5bTJjQmFxS2h5dVF1MDd0TVRMWFRnVW1vN1RNVW5lWm91ZWsxYWRYUzd4M0ZpZVlnRUpSOG8rdlRxVVNJVkkxdjVyYTNpa3d5K1dWK1VsVmJubWlMTHpTR0JHak1mU2NXMVlOcDJKNkVqT2ViNjZDVVRVWjJaYUFQdFk3OXZQaHpRdGdFTkJBQmExR2RhR28rTnVNVkVJOUtwZXhZL3BlbDBYcXpl'
    }
    response = requests.get(url,headers=headers)
    if response.status_code == 200:
        return response
    else:
        return None

def parse_html(response):
    root = etree.HTML(response.text)
    cityList = root.xpath('//div[@class="mp-city-content"]//li[@class="mp-city-item"]/a')
    for city in cityList:
        cityName = city.text
        cityLink = 'https://piao.qunar.com/ticket/list.htm?keyword=%s' % cityName
        wirterRow([
            cityName,
            cityLink
        ])

if __name__ == '__main__':
    url = 'https://piao.qunar.com/daytrip/list.htm'
    init()
    response = get_html(url)
    parse_html(response)