/*
 Navicat Premium Data Transfer

 Source Server         : qunar
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : qunardate

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 09/05/2024 10:19:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for app_user
-- ----------------------------
DROP TABLE IF EXISTS `app_user`;
CREATE TABLE `app_user`  (
  `Uid` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userSex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userAddress` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userAvatar` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userTextarea` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `userCreateTime` datetime(6) NOT NULL,
  PRIMARY KEY (`Uid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of app_user
-- ----------------------------
INSERT INTO `app_user` VALUES (1, 'tengfangxu', 'Qw123456!', '男', '江苏省盐城市', 'avatar/b_28bf85084b37347d5086017fa2d3e86e.jpg', '这个人很懒，什么也没写', '2024-04-09 20:50:11.965964');
INSERT INTO `app_user` VALUES (2, 'tengran', '123456', '', '', 'avatar/default.png', '这个人很懒。什么也没写', '2024-05-08 17:58:31.668219');
INSERT INTO `app_user` VALUES (3, 'root', '123456', '', '', 'avatar/default.png', '这个人很懒。什么也没写', '2024-05-08 18:24:35.111809');

SET FOREIGN_KEY_CHECKS = 1;
