from django.db import models
class TravelInfo(models.Model):
    Jid = models.AutoField(primary_key=True)
    sightName = models.CharField('景点名称', max_length=255, default='')
    star = models.CharField('景点星级', max_length=255, default='')
    city = models.CharField('景点所在城市', max_length=255, default='')
    detailAddress = models.CharField('景点详细地址', max_length=255, default='')
    shortIntro = models.CharField('景点简短介绍', max_length=255, default='')
    detailIntro = models.CharField('景点详情介绍', max_length=2555, default='')
    detailUrl = models.CharField('景点详情网页地址', max_length=255, default='')
    price = models.CharField('景点门票价格', max_length=255, default='')
    discount = models.CharField('景区门票折扣', max_length=255, default='')
    sale_count = models.CharField('景区门票销量', max_length=255, default='')
    score = models.CharField('景点用户评分', max_length=255, default='')
    commentsTotal = models.CharField('景点评论总数', max_length=255, default='')
    comments = models.TextField('景点用户评论', default='')
    cover_img = models.CharField('景点封面图片', max_length=2555, default='')
    img_list = models.CharField('景点图片列表', max_length=2550, default='')
    spiderTime = models.DateTimeField('景点爬取时间', auto_now_add=True)

    class Meta:
        verbose_name_plural = verbose_name = '景区信息'

    def __str__(self):
        return f'{self.Jid}\t{self.sightName}\t{self.star}\t{self.city}\t{self.sale_count}\t{self.score}\t{self.detailAddress}\t{self.detailUrl}'

class User(models.Model):
    Uid = models.AutoField(primary_key=True)
    userName = models.CharField('用户名称', max_length=255, default='')
    password = models.CharField('用户密码', max_length=255, default='')
    userSex = models.CharField('用户性别', max_length=255, default='')
    userAddress = models.CharField('用户地址', max_length=255, default='')
    userAvatar = models.FileField('用户头像', upload_to='avatar', default='avatar/default.png')
    userTextarea = models.CharField('个人简介', max_length=255, default='这个人很懒。什么也没写')
    userCreateTime = models.DateTimeField('注册时间', auto_now_add=True)

    class Meta:
        verbose_name_plural = verbose_name = '用户信息'

    def __str__(self):
        return f'{self.Uid}\t{self.userName}\t{self.password}\t{self.userSex}\t{self.userAddress}\t{self.userTextarea}'
