import time


def spiderMain(self, resp, city):
    respJSON = resp.json()['data']['sightList']
    for index, travel in enumerate(respJSON):
        print('正在爬取该页第%s数据' % str(index + 1))
        time.sleep(2)
        detailAddress = travel['address']
        discount = travel['discount']
        shortIntro = travel['intro']
        price = travel['qunarPrice']
        sale_count = travel['saleCount']
        try:
            star = travel['star'] + '景区'
        except:
            star = '未评价'
        sightName = travel['sightName']
        cover_img = travel['sightImgURL']
        sightId = travel['sightId']
        # ================================= 详情爬取
        detailUrl = self.detailUrl % sightId
        respDetailXpath = etree.HTML(self.send_request(detailUrl).text)
        score = respDetailXpath.xpath('//span[@id="mp-description-commentscore"]/span/text()')
        if not score:
            score = 0
        else:
            score = score[0]
        commentsTotal = respDetailXpath.xpath('//span[@class="mp-description-commentCount"]/a/text()')[0].replace(
            '条评论', '')
        detailIntro = respDetailXpath.xpath('//div[@class="mp-charact-intro"]//p/text()')[0]
        img_list = respDetailXpath.xpath('//div[@class="mp-description-image"]/img/@src')[:6]
        # ================================= 评论爬取
        commentSightId = respDetailXpath.xpath('//div[@class="mp-tickets-new"]/@data-sightid')[0]
        commentsUrl = self.commentUrl % commentSightId
        comments = []
        try:
            commentsList = self.send_request(commentsUrl).json()['data']['commentList']
            for c in commentsList:
                if c['content'] != '用户未点评，系统默认好评。':
                    author = c['author']
                    content = c['content']
                    date = c['date']
                    score = c['score']
                    comments.append({
                        'author': author,
                        'content': content,
                        'date': date,
                        'score': score
                    })
        except:
            comments = []

        resultData = []
        resultData.append(sightName)
        resultData.append(star)
        resultData.append(city)
        resultData.append(detailAddress)
        resultData.append(shortIntro)
        resultData.append(detailIntro)
        resultData.append(detailUrl)
        resultData.append(price)
        resultData.append(discount)
        resultData.append(sale_count)
        resultData.append(score)
        resultData.append(commentsTotal)
        resultData.append(json.dumps(comments))
        resultData.append(cover_img)
        resultData.append(json.dumps(img_list))
        self.save_to_csv(resultData)