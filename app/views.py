from django.shortcuts import render,redirect
from app.models import User,TravelInfo
from app.recommdation import getUser_ratings,user_bases_collaborative_filtering
from app.utils import errorResponse,getHomeData,getPublicData,getChangeSelfInfoData,getChangePassword,getAddCommentsData,getViewCommentsDate,getReviseCommentsData,getDeleteCommentsData,getEchartsData,getRecommendationData
from app.utils.getChangePassword import getChangePassword as getPasswordFunction
from django.http import JsonResponse
import json
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import JsonResponse, HttpResponseRedirect
from django.core.serializers.json import DjangoJSONEncoder

# Create your views here.
def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            User.objects.get(userName=username, password=password)
            request.session['username'] = username
            return redirect('/app/home')

        except:
            return errorResponse.errorResponse(request,'账号或密码错误')

def register(request):
    if request.method == 'GET':
        return render(request,'register.html')
    else:
        username = request.POST.get('username')
        password = request.POST.get('password')
        confirmPassword = request.POST.get('confirmPassword')
        try:
            User.objects.get(userName=username)
        except:
            if not username or not password or not confirmPassword: return errorResponse.errorResponse(request,'不允许为空值')
            if password != confirmPassword: return errorResponse.errorResponse(request, '两次密码不一致')
            User.objects.create(userName=username, password=password)
            return redirect('/app/login')

        return errorResponse.errorResponse(request, '该账号已存在')

def logOut(request):
    request.session.clear()
    return redirect('/app/login')

def home(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    a5Len,commentsTotalTitle,cityDicSort = getHomeData.getHomeTagData()
    scoreTop10Data,sale_countTop = getHomeData.getAnthorData()
    year, mon, day = getHomeData.getNowTime()
    geoData = getHomeData.getGeoData()
    finalResult, topTenCitiesWithScores = getHomeData.getTopTenCities()
    levelData = getHomeData.levelData()
    return render(request, 'home.html', {
        'userInfo': userInfo,
        'a5Len': a5Len,
        'commentsTotalTitle': commentsTotalTitle,
        'cityDicSort': cityDicSort,
        'scoreTop10Data':scoreTop10Data,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'geoData': geoData,
        'sale_countTop': sale_countTop,
        'finalResult': finalResult,
        'topTenCitiesWithScores': topTenCitiesWithScores,
        'levelData':levelData
    })


def displayUserInfo(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()  # 假设这个函数存在并能正确获取时间

    return render(request, 'users-profile.html', {  # 注意这里模板名应与实际模板文件名对应
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
    })
def changeSelfInfo(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year,mon,day = getHomeData.getNowTime()
    if request.method == 'POST':
        getChangeSelfInfoData.changeSelfInfo(username,request.POST,request.FILES)
        userInfo = User.objects.get(userName=username)

    return render(request,'changeSelfInfo.html',{
        'userInfo':userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
    })
def changePassword(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    if request.method == 'POST':
        res = getPasswordFunction(userInfo, request.POST)  # 使用别名调用函数
        if res is not None:
            # 假设res是一个错误信息，转换为包含'message'的字典
            return JsonResponse({'message': res})
        else:
            # 密码更新成功，返回成功标志
            return JsonResponse({'message': '密码更新成功'})
    return render(request, 'changePassword.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
    })

def tableData(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    talbeData = getPublicData.getAllTravelInfoMapData()
    return render(request, 'tableData.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'talbeData':talbeData
    })

def addComments(request,id):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    travelInfo = getAddCommentsData.getTravelById(id)
    if request.method == 'POST':
        getAddCommentsData.addComments({
            'id':id,
            'rate':int(request.POST.get('rate')),
            'content':request.POST.get('content'),
            'userInfo':userInfo,
            'travelInfo':travelInfo
        })
        return redirect('/app/tableData')
    return render(request, 'addComments.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'travelInfo':travelInfo,
        'id':id
    })
def viewComments(request, id):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    travelInfo = getViewCommentsDate.getTravelById(id)
    # 创建分页对象
    paginator = Paginator(travelInfo.comments, 20)  # 每页20条记录
    page_number = request.GET.get('page')
    comments = paginator.get_page(page_number)
    return render(request, 'viewComments.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'travelInfo': travelInfo,
        'comments': comments,
        'id': id
    })

def reviseComments(request, id):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    try:
        travelInfo = getReviseCommentsData.getTravelById(id)
    except Exception as e:  # 捕获可能的异常，例如数据不存在
        return JsonResponse({"error": f"Travel info not found: {e}"}, status=404)

    userComment = next((comment for comment in travelInfo.comments if
                        isinstance(comment, dict) and 'userId' in comment and comment['userId'] == userInfo.Uid), None)

    if request.method == 'POST':
        # 更新评论内容和评分
        if userComment:
            userComment['score'] = int(request.POST.get('score', userComment.get('score', 0)))
            userComment['content'] = request.POST.get('content', userComment.get('content', ''))

            # 确保评论列表中的数据都是有效的字典
            valid_comments = [c for c in travelInfo.comments if
                              isinstance(c, dict) and 'score' in c and 'content' in c]
            # 序列化前验证img_list
            # 在保存之前处理img_list，确保其为有效JSON字符串
            if travelInfo.img_list is not None:
                if isinstance(travelInfo.img_list, list):  # 检查img_list是否为列表，如果是，则序列化
                    travelInfo.img_list = json.dumps(travelInfo.img_list)
                else:
                    try:
                        # 如果img_list已经是字符串，尝试解析以验证其有效性
                        json.loads(travelInfo.img_list)
                    except json.JSONDecodeError:
                        print("Invalid img_list data detected. Resetting to empty list.")
                        travelInfo.img_list = '[]'  # 或者根据需要重置为默认值

            # 序列化并保存更新后的评论列表
            travelInfo.comments = json.dumps(valid_comments, ensure_ascii=False)
            travelInfo.save()

            return redirect('/app/tableData')

    # 如果是GET请求，渲染表单供用户编辑
    return render(request, 'reviseComments.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'comment': userComment,  # 将找到的评论对象传递给模板
        'id': id,  # 传递景点ID，以防万一需要
        'travelInfo': travelInfo
    })

def deleteComments(request, id):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()

    try:
        travelInfo = getDeleteCommentsData.getTravelById(id)
        # 确保comments字段是字符串形式，然后反序列化
        comments_data = json.loads(travelInfo.comments)
    except json.JSONDecodeError:
        return JsonResponse({"error": "Invalid JSON format in comments field"}, status=500)
    except Exception as e:
        return JsonResponse({"error": f"An error occurred: {e}"}, status=500)

    # 使用已反序列化的comments_data来查找当前用户的评论
    userComment = next((comment for comment in comments_data if
                        isinstance(comment, dict) and 'userId' in comment and comment['userId'] == userInfo.Uid), None)

    if request.method == 'POST':
        if userComment:
            # 确保评论存在才尝试删除
            # 从已反序列化的列表中移除指定评论
            comments_data.remove(userComment)

            # 更新后的数据序列化回字符串，以便保存到TextField
            updated_comments_str = json.dumps(comments_data)

            # 更新并保存
            travelInfo.comments = updated_comments_str
            travelInfo.save(update_fields=['comments'])

            return redirect('/app/tableData')  # 删除操作成功后重定向到另一个页面

    # 如果是GET请求，渲染确认删除的页面
    return render(request, 'deleteComments.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'comment': userComment,  # 注意这里的comment可能基于未更新的comments_data，根据需求调整
        'travelInfo': travelInfo,
        'id': id,  # 传递景点ID，以防万一需要
    })
def cityChar(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    cityList = getPublicData.getCityList()
    travelInfoList = getPublicData.getAllTravelInfoMapData(cityList[0])
    Xdata, Ydata = getEchartsData.cityCharDataOne(travelInfoList)
    cityCharDataTwo = getEchartsData.cityCharDataTwo(travelInfoList)
    city_numbers , total_count_value = getHomeData.getcityTagData(travelInfoList)
    if request.method == 'POST':
        travelInfoList = getPublicData.getAllTravelInfoMapData(request.POST.get('city'))
        Xdata, Ydata = getEchartsData.cityCharDataOne(travelInfoList)
        cityCharDataTwo = getEchartsData.cityCharDataTwo(travelInfoList)
        city_numbers , total_count_value = getHomeData.getcityTagData(travelInfoList)
    return render(request, 'cityChar.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'cityList': cityList,
        'cityCharOneData':{
            'Xdata':Xdata,
            'Ydata':Ydata
        },
        'cityCharTwoData':cityCharDataTwo,
        'total_count_value': total_count_value,
        'city_numbers': city_numbers

    })

def rateChar(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    cityList = getPublicData.getCityList()
    travelList = getPublicData.getAllTravelInfoMapData(cityList[0])
    xData,yData = getEchartsData.getRateCharDataOne(travelList)
    charTwoData = getEchartsData.getRateCharDataTwo(travelList)
    allAverageScore, fiveStarCount, fiveStarRatio = getHomeData.allAverageScore(travelList)
    cityAverages = getEchartsData.getRateCharDataThree(travelList)
    x1Data,y1Data = getEchartsData.getRateCharDataFour(travelList)
    if request.method == 'POST':
        travelList = getPublicData.getAllTravelInfoMapData(request.POST.get('city'))
        xData,yData = getEchartsData.getRateCharDataOne(travelList)
        charTwoData = getEchartsData.getRateCharDataTwo(travelList)
        allAverageScore, fiveStarCount, fiveStarRatio = getHomeData.allAverageScore(travelList)
        cityAverages = getEchartsData.getRateCharDataThree(travelList)
        x1Data,y1Data = getEchartsData.getRateCharDataFour(travelList)
    return render(request, 'rateChar.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'cityList': cityList,
        'RateCharDataOne':{
            'xData':xData,
            'yData':yData
        },
        'charTwoData':charTwoData,
        'allAverageScore': allAverageScore,
        'fiveStarCount': fiveStarCount,
        'fiveStarRatio': fiveStarRatio,
        'cityAverages': cityAverages,
        'RateCharDataFour': {
            'x1Data':x1Data,
            'y1Data':y1Data
        },
    })

def priceChar(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    cityList = getPublicData.getCityList()
    travelList = getPublicData.getAllTravelInfoMapData(cityList[0])
    xData,yData = getEchartsData.getPriceCharDataOne(travelList)
    x1Data,y1Data = getEchartsData.getPriceCharDataTwo(travelList)
    disCountPieData = getEchartsData.getPriceCharDataThree(travelList)
    avgPriceAllCities, bestSellingSight = getHomeData.calculateCityAverages(travelList)
    data = getEchartsData.getPriceCharDataFour(travelList)
    json_data = json.dumps(data)
    if request.method == 'POST':
        travelList = getPublicData.getAllTravelInfoMapData(request.POST.get('city'))
        xData, yData = getEchartsData.getPriceCharDataOne(travelList)
        x1Data, y1Data = getEchartsData.getPriceCharDataTwo(travelList)
        disCountPieData = getEchartsData.getPriceCharDataThree(travelList)
        avgPriceAllCities, bestSellingSight = getHomeData.calculateCityAverages(travelList)
        data = getEchartsData.getPriceCharDataFour(travelList)
        json_data = json.dumps(data)
    return render(request, 'priceChar.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'cityList': cityList,
        'echartsData':{
            'xData':xData,
            'yData':yData,
            'x1Data':x1Data,
            'y1Data':y1Data,
            'disCountPieData':disCountPieData
        },
        'avgPriceAllCities':avgPriceAllCities,
        'bestSellingSight':bestSellingSight,
        'json_data': json_data
    })

def commentsChar(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    xData,yData = getEchartsData.getCommentsCharDataOne()
    labels,sizes = getEchartsData.getCommentsCharDataTwo()
    #x1Data,y1Data = getEchartsData.getCommentsCharDataThree()
    return render(request, 'commentsChar.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'echartsData':{
            'xData':xData,
            'yData':yData,
            'labels':labels,
            'sizes':sizes,
            #'x1Data':x1Data,
            #'y1Data':y1Data
        }
    })

def recommendation(request):
    username = request.session.get('username')
    userInfo = User.objects.get(userName=username)
    year, mon, day = getHomeData.getNowTime()
    try:
        user_ratings = getUser_ratings()
        recommended_items = user_bases_collaborative_filtering(userInfo.id, user_ratings)
        resultDataList = getRecommendationData.getAllTravelByTitle(recommended_items)
    except:
        resultDataList = getRecommendationData.getRandomTravel()

    return render(request, 'recommendation.html', {
        'userInfo': userInfo,
        'nowTime': {
            'year': year,
            'mon': mon,
            'day': day
        },
        'resultDataList':resultDataList
    })

