from app.models import User
import re
def getChangePassword(userInfo,passwordInfo):
    oldPwd = passwordInfo['oldPassword']
    newPwd = passwordInfo['newPassword']
    newPwdConfirm = passwordInfo['newPasswordConfirm']

    if len(newPwd) < 8:
        return '新密码必须至少包含8位字符'

        # 验证新密码复杂度
    password_pattern = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$'
    if not re.match(password_pattern, newPwd):
        return '新密码必须包含大写字母、小写字母、数字和特殊符号'

    user = User.objects.get(userName=userInfo.userName)

    if oldPwd != userInfo.password:
        return '原始密码不正确'
    if newPwd != newPwdConfirm:
        return '两次密码输入不一致'

    user.password = newPwd
    user.save()

    return '密码更新成功'