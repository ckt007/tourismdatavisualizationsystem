from app.models import User

def changeSelfInfo(username,formData,file):
    print(formData)
    user = User.objects.get(userName=username)
    user.userAddress = formData['address']
    user.userSex = formData['sex']
    if formData['textarea']:
        user.userTextarea = formData['textarea']
    if file.get('avatar') != None:
        user.userAvatar = file.get('avatar')

    user.save()


