from app.models import TravelInfo
import json

def getTravelById(id):
    travel = TravelInfo.objects.get(Jid=id)
    travel.comments = json.loads(travel.comments)

    return travel
