from app.models import TravelInfo
import json


def getTravelById(id):
    try:
        travel = TravelInfo.objects.get(Jid=id)
        return travel  # 不再在getTravelById中进行反序列化
    except TravelInfo.DoesNotExist:
        return None