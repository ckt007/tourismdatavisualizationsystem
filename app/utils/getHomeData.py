from app.utils import getPublicData
from collections import defaultdict
from statistics import mean
import random
import time
import heapq


travelMapData = getPublicData.getAllTravelInfoMapData()
travelInfoList = getPublicData.getAllTravelInfoMapData()
def getHomeTagData():
    a5Len = 0
    commentsTotalMax = 0
    commentsTotalTitle = ''
    cityDic = {}
    for travel in travelMapData:
        if travel.star == '5A景区':a5Len += 1
        if int(travel.commentsTotal) > commentsTotalMax:
            commentsTotalMax = int(travel.commentsTotal)
            commentsTotalTitle = travel.sightName
        if cityDic.get(travel.city,-1) == -1:cityDic[travel.city] = 1
        else:cityDic[travel.city] += 1

    cityDicSort = list(sorted(cityDic.items(),key=lambda x:x[1],reverse=True))[0][0]
    return a5Len,commentsTotalTitle,cityDicSort


def getAnthorData():
    scoreTop10 = []
    for travel in travelMapData:
        if travel.score == '5':
            scoreTop10.append(travel)

    maxLen = len(scoreTop10)
    scoreTop10Data = []
    for i in range(10):
        randomIndex = random.randint(0, maxLen - 1)
        scoreTop10Data.append(scoreTop10[randomIndex])
    sale_countTop = list(sorted(travelMapData, key=lambda x: int(x.sale_count), reverse=True))[:5047]
    return scoreTop10Data, sale_countTop

def getNowTime():
    timeFormat = time.localtime()
    year = timeFormat.tm_year
    mon = timeFormat.tm_mon
    day = timeFormat.tm_mday
    return year, mon, day

def getGeoData():
    dataDic = {}
    for i in travelMapData:
        for j in getPublicData.cityList:
            for city in j['city']:
                if city.find(i.city) != -1:
                    if dataDic.get(j['province'],-1) == -1:
                        dataDic[j['province']] = 1
                    else:
                        dataDic[j['province']] += 1
    resutData = []
    for key, value in dataDic.items():
        resutData.append({
            'name': key,
            'value': value
        })
    return resutData


def getTopTenCities():
    starCounts = {'3A': {}, '4A': {}, '5A': {}}
    totalCounts = {}
    cityScores = defaultdict(list)  # 存储每个城市的评分列表，用于计算平均分
    # 分别统计每个级别的景区数量
    for travel in travelMapData:
        # 提取星级数字部分，如从"3A景区"提取"3A"
        starLevel = travel.star.split('景区')[0]
        city = travel.city
        score_str = travel.score  # 假设score是字符串类型
        score = float(score_str) if score_str.replace('.', '', 1).isdigit() else 0  # 转换为浮点数，非数字则默认为0
        if city not in totalCounts:
            totalCounts[city] = 0
        totalCounts[city] += 1
        cityScores[city].append(score)  # 收集该城市的评分（已转换为数值）
        if starLevel in starCounts:
            if city not in starCounts[starLevel]:
                starCounts[starLevel][city] = 0
            starCounts[starLevel][city] += 1
    # 计算每个城市的3A+4A+5A总数量
    for city in totalCounts.keys():
        totalCounts[city] = sum(starCounts[star].get(city, 0) for star in ['3A', '4A', '5A'])
    # 按照总数量降序排序并获取前十大城市
    topTenCities = dict(sorted(totalCounts.items(), key=lambda item: item[1], reverse=True)[:10])
    # 计算并添加平均得分
    topTenCitiesWithScores = {}
    for city in topTenCities:
        avg_score = round(sum(cityScores[city]) / len(cityScores[city]), 2) if cityScores[city] else 0
        topTenCitiesWithScores[city] = avg_score
    # 构建最终输出格式
    finalResult = {city: {star: starCounts[star].get(city, 0) for star in ['3A', '4A', '5A']} for city in topTenCities}
    return finalResult, topTenCitiesWithScores
def levelData():
    cityDic = {}
    for travel in travelInfoList:
        if cityDic.get(travel.star, -1) == -1:
            cityDic[travel.star] = 1
        else:
            cityDic[travel.star] += 1
    resultData = []
    for key,value in cityDic.items():
        resultData.append({
            'name':key,
            'value':value
        })
    return resultData

def getcityTagData(travelInfoList):
    global city_numbers
    star_counts = {'3A': 0, '4A': 0, '5A': 0}
    total_counts = {}
    city_count = {}  # 使用字典来存储每个城市的景点计数
    for travel in travelInfoList:
        # 累加每个城市的景点数量
        city_count[travel.city] = city_count.get(travel.city, 0) + 1
        # 计算所有城市的景点数量总和
        city_numbers = sum(city_count.values())
        # 提取星级数字部分，如从"3A景区"提取"3A"
        starLevel = travel.star.split('景区')[0]
        if starLevel in star_counts:
            star_counts[starLevel] += 1

        # 计算所有城市的3A+4A+5A级景点总数量
    total_count_value = sum(star_counts.values())
        # 累加每个城市的景点数量
    return city_numbers , total_count_value

def allAverageScore(travelList):
    totalScore = 0  # 用于累加所有景点的评分
    fiveStarCount = 0  # 用于计数评分为5的景点数量
    validReviewsCount = 0  # 统计有效评分的数量

    # 遍历旅行列表，累加有效评分
    for travel in travelList:
        score_str = travel.score
        score = float(score_str) if score_str.replace('.', '', 1).isdigit() else 0
        if score:  # 确保评分有效（非零）
            totalScore += score
            validReviewsCount += 1
            if score == 5:  # 统计评分为5的景点
                fiveStarCount += 1
    # 计算总体平均分，考虑可能没有有效评分的情况
    allAverageScore = round(totalScore / validReviewsCount, 2) if validReviewsCount > 0 else 0
    # 计算5分景点占比，避免除以零错误
    fiveStarRatio = round(fiveStarCount / validReviewsCount * 100, 2) if validReviewsCount > 0 else 0

    return allAverageScore, fiveStarCount, fiveStarRatio


def calculateCityAverages(travelList):
    global city_numbers
    allPricesSum = 0  # 用于累加所有城市的总价格
    maxSales = 0  # 初始化销量最高值
    bestSellingSight = None  # 初始化销量最高景点名称
    city_count = {}  # 使用字典来存储每个城市的景点计数
    # 遍历旅行列表
    for travel in travelList:
        sale_count_str = str(travel.sale_count)
        price_str = str(travel.price)
        # 累加每个城市的景点数量
        city_count[travel.city] = city_count.get(travel.city, 0) + 1
        # 计算所有城市的景点数量总和
        city_numbers = sum(city_count.values())
        # 尝试将销量和价格转换为浮点数，如果不能转换则跳过此条记录
        try:
            sale_count = float(sale_count_str) if sale_count_str.replace('.', '', 1).isdigit() else 0
            price = float(price_str) if price_str.replace('.', '', 1).isdigit() else 0
        except ValueError:
            continue

        # 累加总价
        allPricesSum += price
        # 更新销量最高的景点信息
        if sale_count > maxSales:
            maxSales = sale_count
            bestSellingSight = travel.sightName  # 假设景点名称存储在travel.sightName

    # 计算所有城市的平均价格
    avgPriceAllCities = round(allPricesSum / city_numbers if city_numbers > 0 else 0, 2)  # 防止除以零错误

    return avgPriceAllCities, bestSellingSight