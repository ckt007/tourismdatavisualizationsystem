import json

from app.utils import getPublicData
from collections import defaultdict
import datetime
travelInfoList = getPublicData.getAllTravelInfoMapData()

def cityCharDataOne(travelInfoList):
    cityDic = {}
    for travel in travelInfoList:
        if cityDic.get(travel.city,-1) == -1:
            cityDic[travel.city] = 1
        else:
            cityDic[travel.city] += 1

    return list(cityDic.keys()),list(cityDic.values())

def cityCharDataTwo(travelInfoList):
    startDic = {}
    for travel in travelInfoList:
        if startDic.get(travel.star, -1) == -1:
            startDic[travel.star] = 1
        else:
            startDic[travel.star] += 1
    resultData = []
    for key, value in startDic.items():
        resultData.append({
            'name': key,
            'value': value
        })
    return resultData
def getRateCharDataOne(travelList):
    # 使用字典存储每个景点的评分
    sightScoreDict = {}
    for travel in travelList:
        sightName = travel.sightName
        score = float(travel.score)

        # 直接记录该景点的评分
        sightScoreDict[sightName] = score

    # 分离出景点名称和对应的评分到两个列表
    xData = list(sightScoreDict.keys())  # 景点名称列表
    yData = list(sightScoreDict.values())  # 评分列表
    return xData, yData

def getRateCharDataTwo(travelList):
    # 初始化各个评分区间的计数器
    scoreRanges = {'0-0.9': 0, '0.9-1.9': 0, '1.9-2.9': 0, '2.9-3.9': 0, '3.9-4.9': 0,'5':0}

    for travel in travelList:
        # 确保score是浮点数类型，如果原本是字符串则转换
        score = float(travel.score)

        # 确定评分所在的区间并增加计数
        if 0 <= score < 0.9:
            scoreRanges['0-0.9'] += 1
        elif 0.9 <= score < 1.9:
            scoreRanges['0.9-1.9'] += 1
        elif 1.9 <= score < 2.9:
            scoreRanges['1.9-2.9'] += 1
        elif 2.9 <= score < 3.9:
            scoreRanges['2.9-3.9'] += 1
        elif 3.9 <= score <= 4.9:
            scoreRanges['3.9-4.9'] += 1
        elif score == 5:
            scoreRanges['5'] +=1

    # 将区间计数转换为饼图所需的数据格式
    resultData = [{'name': range, 'value': count} for range, count in scoreRanges.items() if count > 0]

    return resultData

def getRateCharDataThree(travelList):
    cityScores = defaultdict(lambda: {"totalScore": 0, "validReviewsCount": 0})

    # 遍历旅行列表，累加每个城市的有效评分
    for travel in travelList:
        score_str = travel.score
        score = float(score_str) if score_str.replace('.', '', 1).isdigit() else 0
        if score:  # 确保评分有效（非零）
            cityScores[travel.city]["totalScore"] += score
            cityScores[travel.city]["validReviewsCount"] += 1

    # 计算每个城市的平均分，构建结果字典
    cityAverages = {}
    for city, scores in cityScores.items():
        # 计算平均分，考虑可能没有有效评分的情况
        cityAverage = round(scores["totalScore"] / scores["validReviewsCount"], 2) if scores["validReviewsCount"] > 0 else 0
        cityAverages[city] = cityAverage

    return cityAverages

def getRateCharDataFour(travelList):
    cityFiveStarCounts = defaultdict(int)  # 用于存储每个城市的五分景点数量

    # 遍历旅行列表
    for travel in travelList:
        score_str = travel.score
        score = float(score_str) if score_str.replace('.', '', 1).isdigit() else 0
        # 确保评分有效且为5分
        if score and score == 5:
            # 增加对应城市的五分景点计数
            cityFiveStarCounts[travel.city] += 1

    # 遍历结束后，提取城市名称和对应的五分景点数量
    xData = list(cityFiveStarCounts.keys())  # 城市名称列表
    yData = list(cityFiveStarCounts.values())  # 五分景点数量列表

    return xData, yData

def getPriceCharDataOne(traveList):
    xData = ['免费','100元以内','200元以内','300元以内','400元以内','500元以内','500元以外']
    yData = [0 for x in range(len(xData))]
    for travel in traveList:
        price = float(travel.price)
        if price <= 10:
            yData[0] += 1
        elif price <= 100:
            yData[1] += 1
        elif price <= 200:
            yData[2] += 1
        elif price <= 300:
            yData[3] += 1
        elif price <= 400:
            yData[4] += 1
        elif price <= 500:
            yData[5] += 1
        elif price > 500:
            yData[6] += 1
    return xData,yData

def getPriceCharDataTwo(traveList):
    xData = [str(x * 300) + '份以内' for x in range(1,15)]
    yData = [0 for x in range(len(xData))]
    for travel in traveList:
        sale_count = float(travel.sale_count)
        for x in range(1,15):
            count = x * 300
            if sale_count <= count:
                yData[x - 1] += 1
                break

    return xData,yData


def getPriceCharDataThree(travelList):
    startDic = {}
    for travel in travelList:
        # 特别处理折扣为"10"的字符串情况，其他情况直接使用travel.discount的原始值
        discount_key = '无折扣' if str(travel.discount) == "10" else travel.discount
        if startDic.get(discount_key, -1) == -1:
            startDic[discount_key] = 1
        else:
            startDic[discount_key] += 1

    resultData = []
    for key, value in startDic.items():
        resultData.append({
            'name': key,
            'value': value
        })
    return resultData

def getPriceCharDataFour(travelList):
    city_prices = {}  # 存储每个城市的旅游价格列表
    city_count = {}  # 存储每个城市的景点计数
    # 遍历旅行列表，收集价格数据
    for travel in travelList:
        # 尝试将价格转换为浮点数，如果不能转换则跳过此条记录
        try:
            price = float(str(travel.price)) if str(travel.price).replace('.', '', 1).isdigit() else 0
        except ValueError:
            continue
        # 收集城市价格数据
        city_prices.setdefault(travel.city, []).append(price)
        # 累加每个城市的景点数量
        city_count[travel.city] = city_count.get(travel.city, 0) + 1
    # 准备返回的数据结构
    boxPlotData = {}
    for city, prices in city_prices.items():
        # 对价格列表进行排序，便于计算四分位数等统计量
        prices.sort()
        n = len(prices)
        # 计算四分位数等（这里简单实现，实际中可能需要更精确的处理边缘情况）
        q1 = prices[n // 4] if n > 0 else 0  # 第一四分位数（下四分位数）
        median = prices[n // 2] if n > 0 else 0  # 中位数
        q3 = prices[(3 * n) // 4] if n > 0 else 0  # 第三四分位数（上四分位数）
        # 将每个城市的统计数据添加到返回数据中
        boxPlotData[city] = {
            'min': min(prices) if prices else 0,
            'max': max(prices) if prices else 0,
            'q1': q1,
            'median': median,
            'q3': q3,
            'count': n,
        }
    # 返回数据，包含每个城市的箱型图数据以及所有城市的数量
    return {
        'boxPlotData': boxPlotData,
    }
def getCommentsCharDataOne():
    commentsList = getPublicData.getAllCommentsData()
    xData = []
    def get_list(date):
        return datetime.datetime.strptime(date,'%Y-%m-%d').timestamp()
    for comment in commentsList:
        xData.append(comment['date'])
    xData = list(set(xData))
    xData = list(sorted(xData,key=lambda x: get_list(x),reverse=True))
    yData = [0 for x in range(len(xData))]
    for comment in commentsList:
        for index,date in enumerate(xData):
            if comment['date'] == date:
                yData[index] += 1
    return xData,yData

def getCommentsCharDataTwo():
    travelList = getPublicData.getAllTravelInfoMapData()
    comment_ranges = [(f'{x * 1000}-{(x + 1) * 1000 - 1}条', x) for x in range(19)]
    range_counts = {label: 0 for label, _ in comment_ranges}

    for travel in travelList:
        comment_count = int(travel.commentsTotal)
        for label, index in comment_ranges:
            if index * 1000 <= comment_count < (index + 1) * 1000:
                range_counts[label] += 1
                break

    labels = list(range_counts.keys())
    sizes = list(range_counts.values())

    return labels, sizes

def getCommentsCharDataThree():
    travelList = getPublicData.getAllTravelInfoMapData()
    xData = [str(x * 1000) + '条以内' for x in range(1, 20)]
    yData = [0 for x in range(len(xData))]
    for travel in travelList:
        sale_count = int(travel.commentsTotal)
        for x in range(1, 20):
            count = x * 1000
            if sale_count <= count:
                yData[x - 1] += 1
                break
    return xData, yData