from django.contrib import admin

# Register your models here.
# 配置管理
from .models import TravelInfo,User

class TravelInfoAdmin(admin.ModelAdmin):
    fields = ('Jid','sightName','star','city','sale_count','detailAddress','score','price','commentsTotal','spiderTime','detailUrl')
    list_display = ['Jid','sightName','star','city','sale_count','detailAddress','score','price','commentsTotal','spiderTime','detailUrl']
    search_fields = ['sightName','star','city']
    ordering = ('Jid',)
    list_per_page = 10
    readonly_fields = ('spiderTime','Jid')  # 将createTime字段设置为只读
    list_display_links = ('sightName','Jid')

admin.site.register(TravelInfo,TravelInfoAdmin)
class UserAdmin(admin.ModelAdmin):
    fields = ('Uid','userName','password','userSex','userAddress','userCreateTime')
    list_display = ['Uid','userName','password','userSex','userAddress','userCreateTime']
    search_fields = ['userName','userSex','userAddress']
    ordering = ('Uid',)
    list_per_page = 10
    readonly_fields = ('userCreateTime','Uid')  # 将createTime字段设置为只读
    list_display_links = ('userName','Uid')

admin.site.register(User,UserAdmin)
admin.site.site_header="旅游景点数据分析推荐系统后台管理"