from django.urls import path
from app import views

urlpatterns = [
    path('login/',views.login,name='login'),
    path('register/', views.register, name='register'),
    path('logOut/', views.logOut, name='logOut'),
    path('home/', views.home, name='home'),
    path('users-profile', views.displayUserInfo, name='users-profile'),
    path('changeSelfInfo/', views.changeSelfInfo, name='changeSelfInfo'),
    path('changePassword/', views.changePassword, name='changePassword'),
    path('tableData/', views.tableData, name='tableData'),
    path('addComments/<int:id>', views.addComments, name='addComments'),
    path('viewComments/<int:id>/', views.viewComments, name='viewComments'),
    path('reviseComments/<int:id>', views.reviseComments, name='reviseComments'),
    path('deleteComments/<int:id>', views.deleteComments, name='deleteComments'),
    path('cityChar/', views.cityChar, name='cityChar'),
    path('rateChar/', views.rateChar, name='rateChar'),
    path('priceChar/', views.priceChar, name='priceChar'),
    path('commentsChar/', views.commentsChar, name='commentsChar'),
    path('recommendation/', views.recommendation, name='recommendation'),
]