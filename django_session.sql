/*
 Navicat Premium Data Transfer

 Source Server         : qunar
 Source Server Type    : MySQL
 Source Server Version : 80011
 Source Host           : localhost:3306
 Source Schema         : qunardate

 Target Server Type    : MySQL
 Target Server Version : 80011
 File Encoding         : 65001

 Date: 09/05/2024 10:23:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session`  (
  `session_key` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `session_data` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`) USING BTREE,
  INDEX `django_session_expire_date_a5c62663`(`expire_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of django_session
-- ----------------------------
INSERT INTO `django_session` VALUES ('dbftb63qtuhyqynyahqf4o9ylqukuc06', '.eJxVjMsKwyAQRf_FdZEYx6hddt9vkFHHJH0YSBQKpf_eCNlke88598vqRmvGN7ErK5THhHn8VHZhDmuZXKNujjsU581jeFJuID72ZOFhyWWdPW8KP-jG70uk1-1wTwcTblOrCZPQHQy9VRaRlAAIQNpo2SlLSUK0ZJJV0STjO0IBveoFaA-DJinZ7w_88z_f:1rvlZE:DAWy93v5gmj1Juk6SaR4JoKgxFNRx5bMvBCPh1j_8fQ', '2024-04-27 22:09:48.834467');
INSERT INTO `django_session` VALUES ('fkdorqt27m8q2epvu1rr0zd2bwqydz3b', '.eJxVjEkKwzAQBP8y5yCsUWxLOebuN4gZebxkkcALBEL-Hgl88bWrqr-wr7JEegvcYJM4DhTHzw4X8LRvky_Uz32G-rwxhafEAvpHTpIKKW7LzKoo6qCr6lIvr_vhng4mWqdSY4ukg7XaVOJq1wTRlhBrx21DYipjEFlfmamybcMy1FnIAQaXR4TfH-eWP-c:1s4tPI:0MDNga1c6fmhSb7UuOQt3Klbl7Z6rOfcWglMOqUcTmc', '2024-05-23 02:21:16.352817');

SET FOREIGN_KEY_CHECKS = 1;
